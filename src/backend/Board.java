package backend;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ignacio on 3/22/16.
 */
public class Board {
    private static final int SIZE = 8;
    private Position[][] board;
    private List<Position> lastPositions;
    private List<Position> markedPositions;
    private List<BoardListener> listeners;

    public Board() {
        board = new Position[SIZE][SIZE];
        lastPositions = new ArrayList<>();
        markedPositions = new ArrayList<>();
        listeners = new ArrayList<>();
        for(int i = 0; i<board.length; i++){
            for(int j = 0; j<board[i].length;j++){
                board[i][j] = new Position(i,j);
            }
        }
    }

    public boolean isValidPosition(Position position){
        for(int i = 0; i<board.length; i++){
            for(int j = 0; j<board[i].length;j++){
                if(board[i][j].equals(position)){
                    return true;
                }
            }
        }
        return false;
    }

    public void mark(LinkedStack<Position> positions){
        Position pos = null;
        unmark();
        while(!positions.isEmpty()){
            pos = positions.peek();
            board[pos.getX()][pos.getY()].setMarked(true);
            positions.pop();
            markedPositions.add(pos);
        }
        lastPositions.add(pos);
        onBoardUpdated();
    }

    public void unmark(){
        for(Position p: markedPositions){
            p.setMarked(false);
        }
        markedPositions.clear();
    }

    public List<Position> getLastPositions() {
        return lastPositions;
    }

    public List<Position> getMarkedPositions() {
        return markedPositions;
    }

    public int getSize(){
        return SIZE;
    }

    public void addBoardListener(BoardListener listener){
        listeners.add(listener);
    }

    private void onBoardUpdated(){
        for(BoardListener listener: listeners){
            listener.boardUpdated(markedPositions);
        }
    }
}
