package backend;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by Ignacio on 3/22/16.
 */
public class Simulator {
    private ArrayList<LinkedStack<Position>> stacks;
    private Board board;
    private Piece piece;
    private boolean finished;
    private int numberOfMovements;

    public Simulator(int n) {
        numberOfMovements = n;
        finished = false;
        stacks = new ArrayList<LinkedStack<Position>>();
        board = new Board();
        piece = new Piece(new Position(0,0),2,1);
        for(int i = 0; i < n;i++){
            stacks.add(new LinkedStack<Position>());
        }
        stacks.set(0,piece.getMovements(board));
        fillStacks();
    }

    public LinkedStack<Position> nextMovement(){
        LinkedStack<Position> result = new LinkedStack<Position>();
        if(stacks.get(0).isEmpty()) return null;
        for(int i = stacks.size()-1; i>=0;i--){
            result.push(stacks.get(i).peek());
        }

        for(int j = stacks.size()-1; j>=0;j--){
            stacks.get(j).pop();
            if(!stacks.get(j).isEmpty()){
                break;
            }
        }
        fillStacks();
        return result;
    }

    public void next(){
        LinkedStack<Position> aux = nextMovement();
        if(aux == null){
            System.out.println("Next movement is null ");
            finished = true;
            return;
        }
        board.mark(aux);
    }

    public void fillStacks(){
        for(int i = 1 ; i<stacks.size();i++){
                if (stacks.get(i).isEmpty()) {
                    piece.move(stacks.get(i - 1).peek());
                    stacks.set(i, piece.getMovements(board));
                }
        }

    }

    public Board getBoard() {
        return board;
    }

    public int getNumberOfMovements() {
        return numberOfMovements;
    }

    public boolean isFinished() {
        return finished;
    }

    public List<LinkedStack<Position>> getStacks() {
        List<LinkedStack<Position>> result = new ArrayList<>();
        for(LinkedStack<Position> stack : stacks){
            result.add(copy(stack));
        }
        return result;
    }


    public LinkedStack<Position> copy(LinkedStack<Position> stack){
        LinkedStack<Position> aux = new LinkedStack<Position>();

        while(!stack.isEmpty()){
            aux.push(stack.peek());
            stack.pop();
        }

        LinkedStack<Position> result = new LinkedStack<>();

        while(!aux.isEmpty()){
            result.push(aux.peek());
            stack.push(aux.peek());
            aux.pop();
        }
        return result;
    }

}
