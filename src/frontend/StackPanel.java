package frontend;

import backend.BoardListener;
import backend.LinkedStack;
import backend.Position;
import backend.Simulator;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * Created by Ignacio on 3/25/16.
 */
public class StackPanel extends JPanel implements BoardListener{

    private Simulator simulator;
    private static final int TEXT_HEIGHT = 50;
    private static final int TEXT_WIDTH = 75;
    private int index;
    private int height;

    public StackPanel(Simulator simulator, int index, int height) {
        this.simulator = simulator;
        this.index = index;
        this.height = height;
        simulator.getBoard().addBoardListener(this);

        setLayout(null);
        setSize(TEXT_WIDTH, height);
        setBackground(Color.GRAY);
    }

    @Override
    public void boardUpdated(List<Position> positions) {
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        super.paintComponent(g);
        LinkedStack<Position> stack = simulator.getStacks().get(index);
        int i = 0;
        while(!stack.isEmpty()){
            i++;

            g.drawString(stack.peek().toString(), 20, height - TEXT_HEIGHT*i);
            stack.pop();
        }
        System.out.println("Paint Called "+ index);

    }
}
