package frontend;

import backend.BoardListener;
import backend.Position;
import backend.Simulator;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;


public class BoardFrame extends JFrame{
	private static final long serialVersionUID = 1L;
	
	private static final int CELL_SIZE = 65;
	private static final int COLUMN_WIDTH = 80;

	private BoardPanel boardPanel;
	private Simulator simulator;
	private List<StackPanel> stackPanels;
	private JButton next = new JButton("Next");

	public BoardFrame(Simulator simulator) {
		this.simulator = simulator;

		setLayout(null);
		setSize(simulator.getBoard().getSize() * CELL_SIZE + 80 + simulator.getNumberOfMovements() * COLUMN_WIDTH, simulator.getBoard().getSize() * CELL_SIZE);

		initBoard(simulator);
		initButton(simulator);

		stackPanels = new ArrayList<>();
		for(int i = 0; i < simulator.getNumberOfMovements(); i++){
			StackPanel aux = new StackPanel(simulator,i,simulator.getBoard().getSize() * CELL_SIZE);
			aux.setLocation(simulator.getBoard().getSize() * CELL_SIZE + 80 + i*75,0);
			stackPanels.add(aux);
			add(aux);
		}

	}

	private void initBoard(Simulator simulator) {
		boardPanel = new BoardPanel(simulator.getBoard().getSize(), simulator.getBoard().getSize(), CELL_SIZE, simulator);
		add(boardPanel);
	}

	private void initButton(final Simulator simulator) {
		next.setLocation(simulator.getBoard().getSize() * CELL_SIZE, (simulator.getBoard().getSize() * CELL_SIZE)/2);
		next.setSize(80, 30);
		add(next);

		next.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				simulator.next();
			}
		});
	}
}
