package backend;

/**
 * Created by Ignacio on 3/22/16.
 */
public class Position {
    private int x,y;
    private boolean marked;

    public Position(int x, int y) {
        this.marked = false;
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        if (x != position.x) return false;
        return y == position.y;

    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    public void setMarked(boolean marked){
        this.marked = marked;
    }

    @Override
    public String toString() {
        return "("+ x+ ","+ y+")";
    }
}
