package backend;

import java.util.NoSuchElementException;

/**
 * Created by patrickert on 3/15/16.
 */
public class LinkedStack<K> {
    private Node tope;

    private class Node{
        public K element;
        public Node next;

        public Node(K element) {
            this.element = element;
        }
    }

    public void  push(K k){
        Node aux = new Node(k);
        aux.next = tope;
        tope = aux;
    }
    public void pop(){
        if (tope != null) {
            tope = tope.next;
            return;
        }
    }
    public K peek(){
        if (tope != null){
            return tope.element;
        }
        throw new NoSuchElementException();
    }

    public boolean isEmpty(){
        return tope == null;
    }

}
