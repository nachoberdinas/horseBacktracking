package backend;

/**
 * Created by Ignacio on 3/22/16.
 */
public class Piece {
    private Position position;
    private int displacementY;
    private int displacementX;

    public Piece(Position position, int displacementY, int displacementX) {
        this.position = position;
        this.displacementY = displacementY;
        this.displacementX = displacementX;
    }

    public LinkedStack<Position> getMovements(Board board){
        LinkedStack<Position> positions = new LinkedStack<>();
        LinkedStack<Position> validPosition = new LinkedStack<>();

        positions.push(new Position(position.getX()+displacementX,position.getY()+displacementY));
        positions.push(new Position(position.getX()-displacementX,position.getY()+displacementY));
        positions.push(new Position(position.getX()+displacementX,position.getY()-displacementY));
        positions.push(new Position(position.getX()-displacementX,position.getY()-displacementY));
        positions.push(new Position(position.getX()+displacementY,position.getY()+displacementX));
        positions.push(new Position(position.getX()-displacementY,position.getY()+displacementX));
        positions.push(new Position(position.getX()+displacementY,position.getY()-displacementX));
        positions.push(new Position(position.getX()-displacementY,position.getY()-displacementX));

        while (!positions.isEmpty()){
            if(board.isValidPosition(positions.peek())){
                validPosition.push(positions.peek());
            }
            positions.pop();
        }
        return validPosition;
    }

    public void move(Position position){
        this.position = position;
    }
}
