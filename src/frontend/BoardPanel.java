package frontend;

import backend.BoardListener;
import backend.Position;
import backend.Simulator;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class BoardPanel extends JPanel implements BoardListener {
	private static final long serialVersionUID = 1L;

	private int cellSize;
	private int rows, columns;
	private Image[][] cells;
	private Simulator simulator;
	private Image darkSquare;
	private Image lightSquare;
	private Image redDot;

	private List<backend.Position> lastPositions;
	
	public BoardPanel(final int rows, final int columns, final int cellSize, Simulator simulator) {
		this.rows = rows;
		this.columns = columns;
		this.cellSize = cellSize;
		this.cells = new Image[rows][columns];
		this.simulator = simulator;

		lastPositions = new ArrayList<>();
		loadImages();
		initCells();

		simulator.getBoard().addBoardListener(this);

		setSize(columns * cellSize + 1, rows * cellSize + 1);
		setBackground(Color.DARK_GRAY);
	}

	private void loadImages(){
		try {
			darkSquare = ImageIO.read(new File("DarkSquare.png"));
			lightSquare = ImageIO.read(new File("LightSquare.png"));
			redDot = ImageIO.read(new File("redDot.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void initCells(){
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				setImage(i,j,(i+j)%2==0?darkSquare:lightSquare);
			}
		}
	}

	private void resetCell(int i, int j){

		clearImage(i,j);
		setImage(i,j,(i+j)%2==0?darkSquare:lightSquare);
	}

	public void clearImage(int row, int column) {
		cells[row][column] = null;
	}
	
	public void setImage(int row, int column, Image image) {
		cells[row][column] = new BufferedImage(cellSize, cellSize, BufferedImage.TYPE_INT_ARGB);
		cells[row][column].getGraphics().drawImage(image, 0, 0, null);
		repaint();
	}
	
	public void appendImage(int row, int column, Image image) {
		if (cells[row][column] == null) {
			cells[row][column] = new BufferedImage(cellSize, cellSize, BufferedImage.TYPE_INT_ARGB);
		}
		cells[row][column].getGraphics().drawImage(image, 0, 0, null);
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				if (cells[i][j] != null) {
					g.drawImage(cells[i][j], j * cellSize, i * cellSize, null);
				}
			}
		}
	}

	@Override
	public void boardUpdated(List<backend.Position> positions) {
		for(backend.Position p: lastPositions){
			resetCell(p.getX(),p.getY());
		}
		lastPositions.clear();
		for(Position p: positions){
			lastPositions.add(p);
		}
		for(backend.Position p: positions){
			resetCell(p.getX(),p.getY());
			appendImage(p.getX(),p.getY(),redDot);
		}
		paintImmediately(getBounds());
	}
}