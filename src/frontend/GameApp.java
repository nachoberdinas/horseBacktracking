package frontend;

import backend.Simulator;

import javax.swing.JFrame;

public class GameApp {

	public static void main(String[] args) {
		Simulator simulator = new Simulator(4);
		BoardFrame frame = new BoardFrame(simulator);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}