package backend;

import java.util.List;

/**
 * Created by Ignacio on 3/25/16.
 */
public interface BoardListener {
    public void boardUpdated(List<Position> positions);
}
